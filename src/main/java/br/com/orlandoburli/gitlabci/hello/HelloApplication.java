package br.com.orlandoburli.gitlabci.hello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController("/")
public class HelloApplication {

	public static void main(final String[] args) {
		SpringApplication.run(HelloApplication.class, args);
	}

	@GetMapping("/{name}")
	public String hello(@PathVariable final String name) {
		return String.format("Hello, %s!", name);
	}
}